import { Injectable } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { from, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  isLoggedIn: Observable<any>;

  constructor(

    private nav: NavController,
    private auth: AngularFireAuth,
    private toast: ToastController,
  ) {
      this.isLoggedIn = auth.authState;
   }

  login(user){

    this.auth.signInWithEmailAndPassword(user.email, user.password).
    then(() => this.nav.navigateForward('home')).
    catch(() => this.showError());
  }

  private async showError() {
    const ctrl = await this.toast.create({
      message: 'Dados de acesso incorretos',
      duration: 3000
    });

    ctrl.present();
  }

  recoverPass(data){
      this.auth.sendPasswordResetEmail(data.email).
      then(() => this.nav.navigateBack('auth'));
  }

  createUser(user){
    this.auth.createUserWithEmailAndPassword(user.email, user.password).
    then( credentials => console.log(credentials)).
    catch(err => {
      console.log(err);
    });
  }

  logout(){
    this.auth.signOut().
    then( () => this.nav.navigateBack('auth') );
    
  }

}
