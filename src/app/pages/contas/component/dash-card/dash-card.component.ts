import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'dash-card',
  templateUrl: './dash-card.component.html',
  styleUrls: ['./dash-card.component.scss'],
})
export class DashCardComponent implements OnInit {
  @Input() titulo;
  @Input () icone;
  @Input () tipo;
  @Input() valor;
  @Input() cor;
  @Input () num;

  constructor() { }

  ngOnInit() {
    switch (this.tipo){
      case 'pagar':
        this.pagar();
        break;
      
      case 'receber':
        this.receber();
        break;
      case 'saldo':
        this.saldo();
        break;
        
    }
  }

  pagar(){
    this.titulo = this.titulo ? this.titulo : 'Contas a Pagar';
    this.cor = this.cor ? this.cor : 'danger';
    this.icone = this.icone ? this.icone : 'arrow-down';

  }
  receber(){
    this.titulo = this.titulo ? this.titulo : 'Contas a Receber';
    this.cor = this.cor ? this.cor : 'primary';
    this.icone = this.icone ? this.icone : 'arrow-up';
  }

  saldo(){
    this.titulo = this.titulo ? this.titulo : 'Saldo';
    this.cor = this.cor ? this.cor : 'medium';
    this.icone = this.icone ? this.icone : 'wallet';

  }

}
