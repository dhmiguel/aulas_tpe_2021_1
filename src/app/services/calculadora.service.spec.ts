import { TestBed } from '@angular/core/testing';

import { CalculadoraService } from './calculadora.service';

describe('A classe calculdora', () => {
  let service: CalculadoraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalculadoraService);
  });

  describe('deve realizar divisões', () => {

    it('entre numeros inteiros', () => {
      const result = service.divide(8,4);
      expect(result).toBe(2);
    });

    it('com exceção do zero', () => {
      const result = service.divide(8,0);

      expect(typeof result).toEqual('string');
    });

  });

 
});
